//Holds the DataTable
var table;

$(document).ready( function () {
    table = $('#tblWeather').DataTable();
} );


$('#btnWeather').on('click', function()
{
    $("#tblWeather tbody").empty();
    //Generate 50 days of weather by default
    var numDays = $('#txtNumDays').val();
    if(numDays == "")
    {
        numDays = 50;
    }
    generateWeather(numDays);
    
});

function generateWeather(numDays)
{
    $('#tblWeather').DataTable().clear();

    var weatherDay;
    for(var i = 0; i < numDays; i++)
    {
        //Add the day of the adventure and the associated weather
        table.row.add([i+1, dailyWeather()]);
    }

    table.draw();

}

//Generates the daily weather
function dailyWeather()
{
    var weatherRoll = 0;
    var rainType = 0;
    var tropicalType = 0;
    var weatherString = "";
    //First, Roll 1d10 to determine if it's going to rain.  80% chance each day
    weatherRoll = Math.floor(Math.random() * 10) + 1;
    if(weatherRoll > 2)
    {
        //Next, roll 1d6 to determine the severity of the rain
        rainType = Math.floor(Math.random() * 6) + 1;

        if(rainType == 6)
        {
            //If it's a 6 (heavy rain), roll 1d4 to see if it's a tropical storm
            tropicalType = Math.floor(Math.random() * 4) + 1;
            if(tropicalType == 1)
            {
                weatherString = "Tropical Storm";
            }
            else
            {
                weatherString = "Heavy Rain";
            }
        }
        else
        {
            switch (rainType)
            {
                case 1: 
                    weatherString = "Mist";
                    break;
                case 2:
                    weatherString = "Light Rain";
                    break;
                case 3:
                case 4:
                case 5:
                    weatherString = "Rain";
                    break;
                default:
                    weatherString = "error";
                    break;
            }
        }
    }
    else
    {
        weatherString = "Clear";
    }
    
    return weatherString;
}
