# TombofAnnihilationWeatherGenerator

Generate random weather for DnD 5e's Tomb of Annihilation campaign!  All weather types from the campaign are included!  Search, sort, and pagination all accessible by default!

NOTE: I am terrible at UI/UX.  This is functional, not super-pretty.

To use:
Open up WeatherBuilder.html in your favorite browser
Enter the number of days you want to generate weather for
Press the button

You're done!